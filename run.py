import os
from datetime import datetime, timedelta

from checkin.app import create_app
from checkin.extensions import db
from checkin.models.event import Event
from checkin.models.organization import Organization
from checkin.models.seasonticket import SeasonTicket
from checkin.models.service import Service
from checkin.models.user import User

CONFIG = {
    'development': 'checkin.config.DevelopmentConfig',
    'production': 'checkin.config.BaseConfig'
}

app = create_app(CONFIG[os.environ.get('FLASK_ENV')])


@app.shell_context_processor
def make_shell_context():
    return dict(app=app, db=db)


@app.cli.command()
def populate_db():
    customer = User(login='customer', password='customer', name='Иван Петров')
    customer.save()
    trainer = User(login='trainer', password='trainer', name='Александр Васильев')
    trainer.save()

    organization = Organization(name='Супер Спорт', owner=trainer, info='Мы занимаемся всеми видами спорта.').save()

    service_1 = Service(name='Бег', organization=organization, info='Бегаем от коллекторов вместе.').save()
    service_2 = Service(name='Плавание', organization=organization, info='Плавай лучше чем Титаник.').save()
    service_3 = Service(name='Бокс', organization=organization, info='Если не получилось убежать, ответь.').save()
    service_4 = Service(name='Тенис', organization=organization, info='Учимся мастерски отбиваться от мух.').save()
    service_5 = Service(name='Фитнес', organization=organization, info='Если парень разлюбил, отпусти.').save()

    organization.services.append(service_1)
    organization.services.append(service_2)
    organization.services.append(service_3)
    organization.services.append(service_4)
    organization.services.append(service_5)
    organization.save()

    s_ticket = SeasonTicket(name='Алл инклюзив', organization=organization,
                            services=[service_1, service_2, service_3, service_4, service_5],
                            duration=7776000).save()  # 3 месяца

    customer.season_tickets.create(season_ticket=s_ticket, start=datetime.now(),
                                   end=datetime.now() + timedelta(seconds=s_ticket.duration)).save()

    Event(service=service_1, trainer=trainer, date=datetime.now() + timedelta(days=3),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_2, trainer=trainer, date=datetime.now() + timedelta(days=4),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_3, trainer=trainer, date=datetime.now() + timedelta(days=1),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_4, trainer=trainer, date=datetime.now() + timedelta(days=3),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_5, trainer=trainer, date=datetime.now() + timedelta(days=4),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_3, trainer=trainer, date=datetime.now() + timedelta(days=7),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_5, trainer=trainer, date=datetime.now() + timedelta(days=10),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_1, trainer=trainer, date=datetime.now() + timedelta(days=2),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_3, trainer=trainer, date=datetime.now() + timedelta(days=12),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_2, trainer=trainer, date=datetime.now() + timedelta(days=15),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_4, trainer=trainer, date=datetime.now() + timedelta(days=3),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_5, trainer=trainer, date=datetime.now() + timedelta(days=7),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_1, trainer=trainer, date=datetime.now() + timedelta(days=9),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_2, trainer=trainer, date=datetime.now() + timedelta(days=10),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_3, trainer=trainer, date=datetime.now() + timedelta(days=11),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_4, trainer=trainer, date=datetime.now() + timedelta(days=25),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_5, trainer=trainer, date=datetime.now() + timedelta(days=12),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_3, trainer=trainer, date=datetime.now() + timedelta(days=27),
          address='Улица Пушкина, дом Колотушкина 7').save()
    Event(service=service_2, trainer=trainer, date=datetime.now() + timedelta(days=30),
          address='Улица Пушкина, дом Колотушкина 7').save()
