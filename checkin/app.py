from flask import Flask

from checkin.admin import admin_blueprint
from checkin.api import api_blueprint
from checkin.config import BaseConfig
from checkin.extensions import db, bootstrap


def create_app(config_object=BaseConfig):
    app = Flask(__name__)
    app.config.from_object(config_object)

    register_extensions(app)
    register_blueprints(app)

    return app


def register_extensions(app):
    db.init_app(app)
    bootstrap.init_app(app)


def register_blueprints(app):
    app.register_blueprint(admin_blueprint, url_prefix='/')
    app.register_blueprint(api_blueprint, url_prefix='/api')
