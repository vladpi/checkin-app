from checkin.extensions import db


class Event(db.DynamicDocument):
    service = db.ReferenceField('Service')
    trainer = db.ReferenceField('User')
    date = db.DateTimeField()
    duration = db.LongField()
    address = db.StringField()
    registered = db.ListField(db.ReferenceField('User'))
    attended = db.ListField(db.ReferenceField('User'))
