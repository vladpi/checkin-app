from checkin.extensions import db


class SeasonTicket(db.DynamicDocument):
    name = db.StringField()
    organization = db.ReferenceField('Organization')
    services = db.ListField(db.ReferenceField('Service'))
    count = db.IntField()
    duration = db.LongField()