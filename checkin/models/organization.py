from checkin.extensions import db


class Organization(db.DynamicDocument):
    name = db.StringField()
    owner = db.ReferenceField('User')
    info = db.StringField()
    services = db.ListField(db.ReferenceField('Service'))
