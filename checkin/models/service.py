from checkin.extensions import db


class Service(db.DynamicDocument):
    organization = db.ReferenceField('Organization')
    name = db.StringField()
    info = db.StringField()
