from checkin.extensions import db
from checkin.mixins.user import UserMixin


class UserSeasonTicket(db.DynamicEmbeddedDocument):
    season_ticket = db.ReferenceField('SeasonTicket')
    count = db.IntField()
    start = db.DateTimeField()
    end = db.DateTimeField()


class User(UserMixin, db.DynamicDocument):
    login = db.StringField()
    password = db.StringField()
    secret_pass = db.StringField()

    name = db.StringField()
    season_tickets = db.EmbeddedDocumentListField(UserSeasonTicket)
    events = db.ListField(db.ReferenceField('Event'))
