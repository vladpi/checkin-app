from flask_restful import Resource, reqparse, fields, abort, marshal_with, marshal

from checkin.models.user import User
from checkin.models.event import Event
from checkin.helpers import DateField

user_fields = {
    'id': fields.String(attribute='id'),
    'name': fields.String(attribute=lambda x: x.name or x.login)
}

event_fields = {
    'id': fields.String(attribute='id'),
    'name': fields.String(attribute='service.name'),
    'organization': fields.String(attribute='service.organization.name'),
    'date': DateField(attribute='date'),
    'trainer': fields.String(attribute='trainer.login'),
    'registered_customers': fields.Integer(attribute=lambda x: len(x.registered)),
    'attended_customers': fields.Integer(attribute=lambda x: len(x.attended))
}

customer_event_fields = {
    'id': fields.String(attribute='id'),
    'name': fields.String(attribute='service.name'),
    'organization': fields.String(attribute='service.organization.name'),
    'date': DateField(attribute='date'),
    'trainer': fields.String(attribute='trainer.login'),
    'registered_customers': fields.Integer(attribute=lambda x: len(x.registered)),
    'attended_customers': fields.Integer(attribute=lambda x: len(x.attended)),
    'is_registered': fields.Integer(attribute='is_registered'),
}


class UserLogin(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('login', type=str, required=True)
        self.parser.add_argument('password', type=str, required=True)

        super(UserLogin, self).__init__()

    @marshal_with(user_fields)
    def post(self):
        args = self.parser.parse_args()

        user = User.objects(login=args.login).first()

        if user and args.password == user.password:
            return user

        else:
            abort(401)


class CustomerEvents(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_id', type=str, required=True)
        self.parser.add_argument('start_date', type=int)
        self.parser.add_argument('finish_date', type=int)

        super(CustomerEvents, self).__init__()

    @marshal_with(customer_event_fields)
    def get(self):
        args = self.parser.parse_args()
        user = User.objects.get(id=args.user_id)

        events = user.get_customer_events(start_date=args.start_date, finish_date=args.finish_date)

        return events


class TrainerEvents(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_id', type=str, required=True)
        self.parser.add_argument('start_date', type=int)
        self.parser.add_argument('finish_date', type=int)

        super(TrainerEvents, self).__init__()

    @marshal_with(event_fields)
    def get(self):
        args = self.parser.parse_args()
        user = User.objects.get(id=args.user_id)

        events = user.get_trainer_events(start_date=args.start_date, finish_date=args.finish_date)

        return events


class EventRegister(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_id', type=str, required=True)

        super(EventRegister, self).__init__()

    def post(self, event_id):
        args = self.parser.parse_args()
        customer = User.objects.get(id=args.user_id)
        event = Event.objects.get(id=event_id)

        try:
            customer.register_on_event(event)
            return {'status': 'ok'}, 200

        except ValueError:
            return {'status': 'error'}, 400


class CheckCustomerAttending(Resource):
    def get(self, event_id, user_id):
        customer = User.objects.get(id=user_id)
        event = Event.objects.get(id=event_id)

        if customer.can_attend_on_event(event):
            return marshal(customer, user_fields)

        else:
            abort(401)


class EventAttend(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_id', type=str, required=True)
        self.parser.add_argument('customer_id', type=str, required=True)

        super(EventAttend, self).__init__()

    def post(self, event_id):
        args = self.parser.parse_args()
        trainer = User.objects.get(id=args.user_id)
        customer = User.objects.get(id=args.customer_id)
        event = Event.objects.get(id=event_id)

        if not customer.is_registered_on_event(event):
            customer.register_on_event(event)

        try:
            customer.attend_on_event(trainer, event)
            return {'status': 'ok'}, 200

        except ValueError:
            return {'status': 'error'}, 400
