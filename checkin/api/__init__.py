from flask import Blueprint
from flask_restful import Api


api_blueprint = Blueprint('api', __name__)
api = Api(api_blueprint)

from .routes import *
api.add_resource(UserLogin, '/login')
api.add_resource(CustomerEvents, '/customer/events')
api.add_resource(TrainerEvents, '/trainer/events')
api.add_resource(EventRegister, '/events/<event_id>/register')
api.add_resource(EventAttend, '/events/<event_id>/attend')
api.add_resource(CheckCustomerAttending, '/events/<event_id>/check/<user_id>')