from flask_restful import fields


class DateField(fields.Raw):
    def format(self, value):
        return int(value.strftime('%s'))
