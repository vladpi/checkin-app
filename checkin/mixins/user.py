from datetime import datetime
from pymongo.errors import DuplicateKeyError
from checkin.models.event import Event


class UserMixin:
    def get_customer_events(self, start_date=None, finish_date=None):
        customer_events = []
        for user_season_ticket in self.season_tickets:
            ticket_events = Event.objects(
                service__in=user_season_ticket.season_ticket.services
            ).filter(
                date__gt=user_season_ticket.start,
                date__lt=user_season_ticket.end,
            )

            if start_date:
                start_date = datetime.fromtimestamp(start_date)
                ticket_events = ticket_events.filter(date__gt=start_date)

            if finish_date:
                finish_date = datetime.fromtimestamp(finish_date)
                ticket_events = ticket_events.filter(date__lt=finish_date)

            customer_events += ticket_events

        for event in customer_events:
            event.is_registered = self in event.registered

        return customer_events

    def get_trainer_events(self, start_date=None, finish_date=None):
        trainer_events = Event.objects(trainer=self)

        if start_date:
            start_date = datetime.fromtimestamp(start_date)
            trainer_events = trainer_events.filter(date__gt=start_date)

        if finish_date:
            finish_date = datetime.fromtimestamp(finish_date)
            trainer_events = trainer_events.filter(date__lt=finish_date)

        return list(trainer_events)

    def register_on_event(self, event):
        if self.can_register_on_event(event):
            event.update(add_to_set__registered=self)
            self.update(add_to_set__events=event)

        else:
            raise ValueError

    def can_register_on_event(self, event):
        for user_season_ticket in self.season_tickets:
            if event.service in user_season_ticket.season_ticket.services:
                return True

        return False

    def can_attend_on_event(self, event):
        for user_season_ticket in self.season_tickets:
            if event.service in user_season_ticket.season_ticket.services \
                    and user_season_ticket.start < event.date < user_season_ticket.end:
                return True

        return False

    def is_registered_on_event(self, event):
        return self in event.registered

    def attend_on_event(self, trainer, event):
        if event.trainer == trainer and self in event.registered:
            event.update(add_to_set__attended=self)

        else:
            raise ValueError
