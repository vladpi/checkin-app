from random import randint
from checkin.models.event import Event


def get_metric_by_month(organization):
    events = Event.objects(service__in=organization.services)

    months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август',
              'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']

    registrations_by_month = [randint(60, 250) for _ in range(12)]
    attends_by_month = [registrations_by_month[i] - randint(5, 35) for i in range(12)]

    count_tickets_by_month = [registrations_by_month[i] - randint(30, 50) for i in range(12)]
    money_by_month = [count_tickets_by_month[i] * 3000 for i in range(12)]

    total_tickets = sum(count_tickets_by_month)
    total_clients = int(total_tickets / randint(2, 5))
    avg_money = int(sum(money_by_month) / len(money_by_month))

    for event in events:
        registrations_by_month[event.date.month] += len(event.registered) * 10
        attends_by_month[event.date.month] += len(event.attended) * 10

    return months, registrations_by_month, attends_by_month, count_tickets_by_month, \
           money_by_month, total_tickets, total_clients, avg_money
