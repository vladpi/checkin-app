import pygal
from flask import render_template
from pygal.style import RotateStyle

from checkin.admin import admin_blueprint
from checkin.models.organization import Organization
from checkin.models.user import User
from .helpers import get_metric_by_month


@admin_blueprint.route('/')
def index():
    user = User.objects.get(login='trainer')

    metrics = get_metric_by_month(Organization.objects.get(owner=user))
    months, registrations_by_month, attends_by_month, count_tickets_by_month, money_by_month, \
    total_tickets, total_clients, avg_money = metrics

    bar_style = RotateStyle('#662D91')
    bar_style.background = '#FFFFFF'
    bar_chart_1 = pygal.Bar(title='Абонементы, регистрации и посещения за месяц', height=400,
                            style=bar_style)
    bar_chart_1.x_labels = months
    bar_chart_1.add('Абонементы', count_tickets_by_month)
    bar_chart_1.add('Регистраций', registrations_by_month)
    bar_chart_1.add('Посещений', attends_by_month)

    bar_chart_1 = bar_chart_1.render_data_uri()

    bar_chart_2 = pygal.Bar(title='Доходы', height=400,
                            style=bar_style)
    bar_chart_2.x_labels = months
    bar_chart_2.add('Доход (₽)', money_by_month)
    bar_chart_2 = bar_chart_2.render_data_uri()

    return render_template('index.html', user=user,
                           total_tickets=total_tickets, total_clients=total_clients, avg_money=avg_money,
                           bar_chart_1=bar_chart_1, bar_chart_2=bar_chart_2)
