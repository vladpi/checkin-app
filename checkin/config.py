import os


class BaseConfig(object):
    DEBUG = False
    SECRET_KEY = os.environ.get('SECRET_KEY')
    MONGODB_URI = os.environ.get('MONGODB_URI')
    # REDIS_URI = os.environ.get('REDIS_URL')

    MONGODB_SETTINGS = {
        'host': MONGODB_URI
    }

    # CELERY_BROKER_URL = REDIS_URI
    # CELERY_RESULT_BACKEND = REDIS_URI

    # JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY')
    # JWT_TOKEN_LOCATION = ['cookies', 'headers']
    # JWT_ACCESS_COOKIE_PATH = '/'
    # JWT_REFRESH_COOKIE_PATH = '/core/token/refresh'
    # JWT_COOKIE_CSRF_PROTECT = False


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    FLASK_DEBUG = True
    SECRET_KEY = 'dev-secret-key'
    MONGODB_URI = 'mongodb://localhost:27017/hack'
    REDIS_URI = 'redis://localhost:6379/0'

    MONGODB_SETTINGS = {
        'host': MONGODB_URI
    }

    CELERY_BROKER_URL = REDIS_URI
    CELERY_RESULT_BACKEND = REDIS_URI

    # JWT_SECRET_KEY = 'jwt-secret-key'
